package buu.thodsaporn.androidtask2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ScoreViewModelFactory (private val correct: Int, private val incorrect: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(correct, incorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}