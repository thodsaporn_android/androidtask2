package buu.thodsaporn.androidtask2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.thodsaporn.androidtask2.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        val binding : FragmentTitleBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_title,
            container,
            false)
        val args = TitleFragmentArgs.fromBundle(requireArguments())

        binding.playPlusGamebtn.setOnClickListener { view ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                    args.pointCorrect,
                    args.pointIncorrect
                )
            )
        }
        binding.playMinusGamebtn.setOnClickListener { view ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToMinusGameFragment(
                    args.pointCorrect,
                    args.pointIncorrect
                )
            )
        }
        binding.playMultiGamebtn.setOnClickListener { view ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToMultiplyGameFragment(
                    args.pointCorrect,
                    args.pointIncorrect
                )
            )
        }
        binding.playDivideGamebtn.setOnClickListener { view ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToDivideGameFragment(
                    args.pointCorrect,
                    args.pointIncorrect
                )
            )
        }
        return binding.root
    }






}