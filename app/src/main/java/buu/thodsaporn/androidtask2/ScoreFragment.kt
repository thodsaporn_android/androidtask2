package buu.thodsaporn.androidtask2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import buu.thodsaporn.androidtask2.databinding.FragmentScoreBinding
import buu.thodsaporn.androidtask2.plus.PlusGameFragmentArgs

class ScoreFragment : Fragment() {
    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentScoreBinding>(inflater, R.layout.fragment_score, container, false)
        viewModelFactory = ScoreViewModelFactory(
            PlusGameFragmentArgs.fromBundle(requireArguments()).pointCorrect,
            PlusGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)

        viewModel.correct.observe(viewLifecycleOwner, Observer { newCorrect ->
            binding.txtCorrect.text = getString(R.string.correct, newCorrect)
        })
        viewModel.incorrect.observe(viewLifecycleOwner, Observer { newIncorrect ->
            binding.txtIncorrect.text = getString(R.string.incorrect, newIncorrect)
        })

        binding.btnMenu.setOnClickListener { view ->
            view.findNavController().navigate(
                ScoreFragmentDirections.actionScoreFragmentToTitleFragment(0,0))
        }

        binding.lifecycleOwner = this

        return binding.root
    }
}