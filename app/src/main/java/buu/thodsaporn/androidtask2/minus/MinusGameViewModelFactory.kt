package buu.thodsaporn.androidtask2.minus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MinusGameViewModelFactory (private val finalPointCorrect: Int, private val finalPointIncorrect: Int ) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MinusGameViewModel::class.java)) {
            return MinusGameViewModel(finalPointCorrect,finalPointIncorrect) as T
        }
        throw IllegalAccessException("Unknown ViewModel Class")
    }
}