package buu.thodsaporn.androidtask2.minus

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.thodsaporn.androidtask2.R
import buu.thodsaporn.androidtask2.databinding.FragmentMinusGameBinding

class MinusGameFragment : Fragment() {
    private lateinit var viewModel: MinusGameViewModel
    private lateinit var binding: FragmentMinusGameBinding
    private lateinit var viewModelFactory: MinusGameViewModelFactory

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_minus_game,
            container,
            false)
        viewModelFactory = MinusGameViewModelFactory(
            MinusGameFragmentArgs.fromBundle(requireArguments()).pointCorrect,
            MinusGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MinusGameViewModel::class.java)

        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = MinusGameFragmentDirections.actionMinusGameFragmentToScoreFragment(
                    viewModel.pointCorrect.value ?: 0,
                    viewModel.pointIncorrect.value ?: 0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        binding.minusGameViewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.play(binding)

        return binding.root
    }



}