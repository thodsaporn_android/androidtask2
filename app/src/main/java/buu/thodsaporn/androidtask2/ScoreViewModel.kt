package buu.thodsaporn.androidtask2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel (correctScore:Int, incorrectScore: Int) : ViewModel() {
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    init {
        _correct.value = correctScore
        _incorrect.value = incorrectScore
    }
}