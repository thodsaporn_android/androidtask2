package buu.thodsaporn.androidtask2.plus

import android.util.Log
import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.thodsaporn.androidtask2.databinding.FragmentMinusGameBinding
import buu.thodsaporn.androidtask2.databinding.FragmentPlusGameBinding
import kotlin.random.Random


class GameViewModel(finalPointCorrect: Int, finalPointIncorrect: Int) : ViewModel() {

    private val _pointIncorrect = MutableLiveData<Int>()
    val pointIncorrect : LiveData<Int>
        get() = _pointIncorrect

    private val _pointCorrect = MutableLiveData<Int>()
    val pointCorrect : LiveData<Int>
        get() = _pointCorrect

    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish : LiveData<Boolean>
        get() = _eventGameFinish

    init {
        _pointCorrect.value = finalPointCorrect
        _pointIncorrect.value = finalPointIncorrect
        Log.i("GameViewModel", "GameViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    fun correct() {
        _pointCorrect.value = _pointCorrect.value?.plus(1)
    }

    fun incorrect() {
        _pointIncorrect.value = _pointIncorrect.value?.plus(1)
    }

    fun onGameFinish() {
        _eventGameFinish.value = true
    }

    fun play(binding: FragmentPlusGameBinding) {
        val result = setQuestion(binding)
        val btn1 = binding.btnAns1
        val btn2 = binding.btnAns2
        val btn3 = binding.btnAns3

        randomButton(result, btn1, btn2, btn3)

        checkClick(btn1, result, binding)
        checkClick(btn2, result, binding)
        checkClick(btn3, result, binding)
    }

    private fun checkClick(
        btn: Button,
        result: Int,
        binding: FragmentPlusGameBinding

    ) {
        btn.setOnClickListener {
            if (btn.text.toString().toInt() == result) {
                correct()
            } else {
                incorrect()
            }
            play(binding)
        }
    }

    private fun randomButton(
        result: Int,
        btn1: Button,
        btn2: Button,
        btn3: Button
    ) {
        val randomNum = Random.nextInt(1, 4)
        val btnValue1 = (result + 1).toString()
        val btnValue2 = (result - 1).toString()
        when (randomNum) {
            1 -> {
                btn1.text = result.toString()
                btn2.text = btnValue1
                btn3.text = btnValue2

            }
            2 -> {
                btn1.text = btnValue1
                btn2.text = result.toString()
                btn3.text = btnValue2
            }
            else -> {
                btn1.text = btnValue1
                btn2.text = btnValue2
                btn3.text = result.toString()
            }
        }
    }

    private fun setQuestion(binding: FragmentPlusGameBinding): Int {
        val num1 = binding.num1
        val num2 = binding.num2
        val result : Int
        val number1 = Random.nextInt(0, 10)
        val number2 = Random.nextInt(0, 10)

        result = number1 + number2

        num1.text = number1.toString()
        num2.text = number2.toString()
        return result
    }

}