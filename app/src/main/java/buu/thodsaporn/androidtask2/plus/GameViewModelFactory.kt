package buu.thodsaporn.androidtask2.plus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class GameViewModelFactory (private val finalPointCorrect: Int,private val finalPointIncorrect: Int ) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
            return GameViewModel(finalPointCorrect,finalPointIncorrect) as T
        }
        throw IllegalAccessException("Unknown ViewModel Class")
    }
}