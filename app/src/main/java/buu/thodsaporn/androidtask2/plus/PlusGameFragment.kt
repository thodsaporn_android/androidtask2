package buu.thodsaporn.androidtask2.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.thodsaporn.androidtask2.R
import buu.thodsaporn.androidtask2.databinding.FragmentPlusGameBinding

class PlusGameFragment : Fragment() {

    private lateinit var binding: FragmentPlusGameBinding
    private lateinit var viewModel: GameViewModel
    private lateinit var viewModelFactory: GameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plus_game, container, false)

        viewModelFactory = GameViewModelFactory(
            PlusGameFragmentArgs.fromBundle(requireArguments()).pointCorrect,
            PlusGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        )

        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)

        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = PlusGameFragmentDirections.actionPlusGameFragmentToScoreFragment(
                    viewModel.pointCorrect.value ?: 0,
                    viewModel.pointIncorrect.value ?: 0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        binding.gameViewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.play(binding)

        return binding.root
    }



}