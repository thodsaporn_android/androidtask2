package buu.thodsaporn.androidtask2.divide

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.thodsaporn.androidtask2.R
import buu.thodsaporn.androidtask2.databinding.FragmentDivideGameBinding


class DivideGameFragment : Fragment() {
    private lateinit var viewModel: DivideGameViewModel
    private lateinit var binding: FragmentDivideGameBinding
    private lateinit var viewModelFactory: DivideGameViewModelFactory

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_divide_game,
            container,
            false)
        viewModelFactory = DivideGameViewModelFactory(
            DivideGameFragmentArgs.fromBundle(requireArguments()).pointCorrect,
            DivideGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(DivideGameViewModel::class.java)

        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = DivideGameFragmentDirections.actionDivideGameFragmentToScoreFragment(
                    viewModel.pointCorrect.value ?: 0,
                    viewModel.pointIncorrect.value ?: 0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        binding.divideGameViewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.play(binding)

        return binding.root
    }

}