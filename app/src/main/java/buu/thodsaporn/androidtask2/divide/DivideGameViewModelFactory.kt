package buu.thodsaporn.androidtask2.divide

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DivideGameViewModelFactory (private val finalPointCorrect: Int, private val finalPointIncorrect: Int ) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DivideGameViewModel::class.java)) {
            return DivideGameViewModel(finalPointCorrect,finalPointIncorrect) as T
        }
        throw IllegalAccessException("Unknown ViewModel Class")
    }
}