package buu.thodsaporn.androidtask2.multiply

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.thodsaporn.androidtask2.R
import buu.thodsaporn.androidtask2.databinding.FragmentMultiplyGameBinding

class MultiplyGameFragment : Fragment() {
    private lateinit var viewModel: MultiplyGameViewModel
    private lateinit var binding: FragmentMultiplyGameBinding
    private lateinit var viewModelFactory: MultiplyGameViewModelFactory

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_multiply_game,
            container,
            false)
        viewModelFactory = MultiplyGameViewModelFactory(
            MultiplyGameFragmentArgs.fromBundle(requireArguments()).pointCorrect,
            MultiplyGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MultiplyGameViewModel::class.java)

        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action =
                    MultiplyGameFragmentDirections.actionMultiplyGameFragmentToScoreFragment(
                        viewModel.pointCorrect.value ?: 0,
                        viewModel.pointIncorrect.value ?: 0
                    )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })
        binding.multiplyGameViewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.play(binding)

        return binding.root
    }
}