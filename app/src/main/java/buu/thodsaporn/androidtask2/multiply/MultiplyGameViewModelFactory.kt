package buu.thodsaporn.androidtask2.multiply

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MultiplyGameViewModelFactory (private val finalPointCorrect: Int, private val finalPointIncorrect: Int ) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultiplyGameViewModel::class.java)) {
            return MultiplyGameViewModel(finalPointCorrect,finalPointIncorrect) as T
        }
        throw IllegalAccessException("Unknown ViewModel Class")
    }
}